# Music Genre Prediction

## Intro 

In this project two approaches are proposed based on different audio extraction methods.\
The first extracts the raw waveform from the *mp3* files, generating a one dimensional signal of fixed length which is
 then fed to a neural network made of 1D convolutional layers.\
The second approach extracts the spectogram of the audio files and generates an image for each of them. In this way it is
 possible to use a canonical 2D-CNN, usually applied in the field of Computer Vision.
 
## References
In the [Journals](Journals) directory you can find various papers and references to the architecture and to the dataset.

## Report
The report can be found in the  [Journals](Journals/report.pdf) direcotry and it contains a detailed analysis of the entire work flow.

# Setup

## Installing Requirements
First you need to install the requirements using \
`pip install -r requirements.txt`\
It is strongly advice to have the GPU version of tensorflow

## Generating Folders
Then you need to run the Path file:\
`python Path.py`\
in order to auto-generate the required folders. 

## Dataset 
Then you need to download from the [FMA](https://github.com/mdeff/fma) both the dataset and the metadata in the 
*Dataset/music_dataset* directory and unzip them.

# Branches
Since the two proposed architectures are very different from each other, 
we decided to create two separated Git branches.\
In order to use the Waveform architecture for the raw audio input it is necessary to move
into the *master* branch. For the Spectogram architecture, instead, it is required to
move on the *code-integration* branch.

### Hyperparameters
You can find the [Hyperparameters](src/model/hyperparams.py) file in which you can set various configurations for the model.
In order to train you must set the *mode* flag to **TRAIN**. If, instead,
you want to make a prediction on a new song the *mode* flag must bet set to **PREDICT**.


# Execution
In order to run the process it is necessary to run the main file\
`python main.py`\
from the [src](src) directory.\
This command is valid for both branches. In particular it will generate the dataset
if not already previously generated. When finished, it will launch the training process
or the prediction procedure, depending on the *mode* flag.

## Data generation
Almost all the steps for the data generation can be interrupted and restored later on. This is not true for the TFRecords writing.\
Stopping the process during the generation of the TFRecords will result in incomplete iterator later on, that will return 
exceptions during the training phase. So if you encouter such exceptions delete the TFRecords in their [directory](Dataset/Custom/Tensorflow) and relaunch the training.

## Training
As said before the *mode* must be set to **TRAIN**. Running the main will start the
training procedure.\
After every epoch the model weights will be saved and can be restored in future trainings or 
preditions setting the *load_model* to True in the [Hyperparameters](src/model/hyperparams.py) file.\
In this file you can also set the number of epochs for the training phase


## Prediction
In order to predict the genre of a new song it is necessary to move the *mp3* of the track
in the "Debug/song_to_predict/" folder.\
 When the *mode*
flag is set to **PREDICT** it will generate the spectogram, the slices and generate the
TFRecords for that song. Afterwards it will feed this last to the network in 
evaluation mode (it will not train of course) and print the genre prediction for
every slice of the song with its prediction confidence.\
Finally it will output the genre prediction of the whole song counting the most
frequent genre among the genre predictions of the single chunks.


# Authors
The authors of this repo are [Nicolo' Brandizzi](https://www.linkedin.com/in/nicol%C3%B2-brandizzi-04091b153/) and [Eric Stefan Miele](https://www.linkedin.com/in/ericstefanmiele/)



