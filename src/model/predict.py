import tensorflow as tf

from Path import Path
from model.evaluate import eval_on_dev
from model.hyperparams import Hyperparams as hp
from model.model import Model
import numpy as np
from data.signal_processing import extract_audio_features
from data.generation import npy_to_tfrecords_single_sample
from Utils.serialization import load_pkl


def predict(config, features_shape, num_labels):
    print(f'Number of labels: {num_labels}')

    features = extract_audio_features(Path.song_to_predict)
    print(features.shape)

    idx2genre = load_pkl(Path.genre_dict)[1]

    npy_to_tfrecords_single_sample(features, Path.tfr_song_to_predict)

    # Define current graph
    graph = tf.Graph()

    with tf.Session(graph=graph, config=config) as sess:
        # Open writer and add graph.

        model = Model(graph, features_shape[1], training=True)

        try:
            model.saver.restore(sess, tf.train.latest_checkpoint(Path.CHKP_DIR))
            sess.run(tf.local_variables_initializer())
            print("Model successfully loaded.")
        except Exception as e:
            print(e)
            print("Unable to load model.")
            exit(0)

        sess.run(model.global_step.initializer)

        sess.run(model.iterator_init_op)

        with tf.device("/gpu:0"):
            prediction = sess.run(model.predictions)
            print(prediction)
            genre_idx = np.argmax(prediction)
            genre = idx2genre[genre_idx]
            print(f'The predicted genre is: {genre}')

