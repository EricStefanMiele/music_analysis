import tensorflow as tf

from Path import Path
from model.evaluate import eval_on_dev
from model.hyperparams import Hyperparams as hp
from model.model import Model
import numpy as np


def train(config, features_shape, num_labels):
    print(f'Train features shape: {features_shape}')
    # print(f'Train track paths shape: {track_paths.shape}')

    print(f'Number of labels: {num_labels}')

    # Define current graph
    graph = tf.Graph()

    with tf.Session(graph=graph, config=config) as sess:
        # Open writer and add graph.

        model = Model(graph, features_shape[1], training=True)

        if hp.load_model:
            try:
                model.saver.restore(sess, tf.train.latest_checkpoint(Path.CHKP_DIR))
                sess.run(tf.local_variables_initializer())
                print("Model successfully loaded.")
            except Exception as e:
                print(e)
                print("Unable to load model, initializing variables from scratch.")
                sess.run(model.init)
        else:
            # Initialize all variables if model is not loaded
            sess.run(model.init)

            print("Model NOT loaded, just doing a normal run.")
        sess.run(model.global_step.initializer)

        for epoch in range(hp.epochs):
            sess.run(model.iterator_init_op)

            step = 0
            loss_sum = 0
            saving_step = 70

            auc_roc_all = list()
            auc_pr_all = list()

            num_correct_predictions = 0
            num_total_predictions = 0

            # use try/except with tensorflow iterator
            while True:
                with tf.device("/gpu:0"):
                    try:
                        _, batch_loss, auc_roc, auc_pr, correct_preds, num_preds = sess.run(
                            [model.train_op, model.loss, model.auc_roc, model.auc_pr, model.correct_predictions, model.num_predictions])
                        # feat,label= sess.run([model.features,model.labels])
                        # if step==2: raise tf.errors.OutOfRangeError("piscia","sta","merda")
                    except tf.errors.OutOfRangeError:
                        # when iterator has no more objects
                        print("\nEpoch concluded")
                        # save model
                        model.save_model(sess)
                        # call eval
                        eval_on_dev(config, features_shape)
                        print()
                        break

                    loss_sum += batch_loss
                    avg_loss = loss_sum / (step + 1)

                    auc_roc_all.append(float(auc_roc))
                    auc_pr_all.append(float(auc_pr))

                    num_correct_predictions += correct_preds
                    num_total_predictions += num_preds

                    # if time to save
                    if step % saving_step == 0:
                        feed_dict={
                            model.avg_loss:avg_loss,
                            model.auc_roc_ph:np.mean(auc_roc_all),
                            model.auc_pr_ph:np.mean(auc_pr_all),
                            model.accuracy_ph: num_correct_predictions/num_total_predictions,

                        }
                        # call log method
                        model.log_scores(sess, feed_dict, train_flag=True)

                    print(
                        f"\rTRAIN Epoch: {epoch + 1} - Step {step + 1} - "
                        + f"Avg train loss: {round(avg_loss, 4)}"
                        + f" - ACCURACY train: {round(num_correct_predictions/num_total_predictions * 100, 2)}%"
                        + f" - AUC ROC: {round(np.mean(auc_roc_all), 4)}"
                        + f" - AUC PR: {round(np.mean(auc_pr_all), 4)}",
                        end="",
                    )
                    step += 1
