import tensorflow as tf
from tensorflow.python import keras

from model.hyperparams import Hyperparams as hp

'''
models.py: in this script some tensorflow models are build.
See build_model() for an example showing how to use these functions.
'''


def wave_frontend(x, is_training):
    '''Function implementing the front-end proposed by Lee et al. 2017.
       Lee, et al. "Sample-level Deep Convolutional Neural Networks for Music Auto-tagging Using Raw Waveforms." 
       arXiv preprint arXiv:1703.01789 (2017).
    - 'x': placeholder whith the input.
    - 'is_training': placeholder indicating weather it is training or test phase, for dropout or batch norm.
    '''

    for i in range(len(hp.filters_fe)):
        x = tf.layers.conv1d(
            inputs=x,
            kernel_size=3,
            filters=hp.filters_fe[i],
            strides=hp.strides_fe[i],
            padding="valid",
            activation=tf.nn.elu,
            kernel_initializer=tf.contrib.layers.xavier_initializer()
        )
        x = tf.layers.batch_normalization(
            inputs=x,
            training=is_training,
            trainable=True
        )

        x = tf.layers.max_pooling1d(
            inputs=x,
            pool_size=3,
            strides=3
        )

        if is_training:
            x = tf.nn.dropout(
                x=x,
                keep_prob=1-hp.dropout_fe
            )

    return tf.expand_dims(x, [3])


def backend(route_out, is_training, config, num_units):
    '''Function implementing the proposed back-end.
    - 'route_out': is the output of the front-end, and therefore the input of this function.
    - 'is_training': placeholder indicating weather it is training or test phase, for dropout or batch norm.
    - 'config': dictionary with some configurable parameters like: number of output units - config['numOutputNeurons']
                or number of frequency bins of the spectrogram config['setup_params']['yInput']
    - 'num_units': number of units/neurons of the output dense layer.
    '''

    # conv layer 1 - adapting dimensions
    conv1 = keras.layers.Conv2D(filters=512,
                                kernel_size=[7, route_out.shape.as_list()[2]],
                                padding="valid",
                                activation=tf.nn.elu,
                                name='1cnnOut',
                                kernel_initializer=tf.contrib.layers.xavier_initializer())(route_out)

    bn_conv1 = keras.layers.BatchNormalization()(conv1, training=True)

    if is_training:
        bn_conv1 = keras.layers.Dropout(rate=hp.dropout_be)(bn_conv1)

    bn_conv1_t = tf.transpose(bn_conv1, [0, 1, 3, 2])

    # conv layer 2 - residual connection
    bn_conv1_pad = tf.pad(bn_conv1_t, [[0, 0], [3, 3], [0, 0], [0, 0]], "CONSTANT")
    conv2 = keras.layers.Conv2D(filters=512,
                                kernel_size=[7, bn_conv1_pad.shape.as_list()[2]],
                                padding="valid",
                                activation=tf.nn.elu,
                                name='2cnnOut',
                                kernel_initializer=tf.contrib.layers.xavier_initializer())(bn_conv1_pad)

    conv2_t = tf.transpose(conv2, [0, 1, 3, 2])
    bn_conv2 = keras.layers.BatchNormalization()(conv2_t, training=True)

    if is_training:
        bn_conv2 = keras.layers.Dropout(rate=hp.dropout_be)(bn_conv2)

    res_conv2 = tf.add(bn_conv2, bn_conv1_t)

    # temporal pooling
    pool1 = keras.layers.MaxPooling2D(pool_size=[2, 1], strides=[2, 1], name='poolOut')(inputs=res_conv2)

    # conv layer 3 - residual connection
    bn_conv4_pad = tf.pad(pool1, [[0, 0], [3, 3], [0, 0], [0, 0]], "CONSTANT")

    conv5 = keras.layers.Conv2D(filters=512,
                                kernel_size=[7, bn_conv4_pad.shape.as_list()[2]],
                                padding="valid",
                                activation=tf.nn.elu,
                                name='3cnnOut',
                                kernel_initializer=tf.contrib.layers.xavier_initializer())(bn_conv4_pad)

    conv5_t = tf.transpose(conv5, [0, 1, 3, 2])
    bn_conv5 = keras.layers.BatchNormalization()(conv5_t, training=True)

    if is_training:
        bn_conv5 = keras.layers.Dropout(rate=hp.dropout_be)(bn_conv5)

    res_conv5 = tf.add(bn_conv5, pool1)

    # global pooling: max and average
    max_pool2 = tf.reduce_max(res_conv5, axis=1)
    avg_pool2, var_pool2 = tf.nn.moments(res_conv5, axes=[1])
    pool2 = tf.concat([max_pool2, avg_pool2], 2)

    # flat_pool2 = tf.contrib.layers.flatten(pool2)
    flat_pool2 = keras.layers.Flatten()(pool2)

    # output - 1 dense layer with droupout
    if is_training:
        flat_pool2 = keras.layers.Dropout(rate=hp.dense_dropout)(flat_pool2)

    dense = keras.layers.Dense(
        units=num_units,
        activation=tf.nn.elu,
        kernel_initializer=tf.contrib.layers.xavier_initializer())(inputs=flat_pool2)
    bn_dense = keras.layers.BatchNormalization()(dense, training=True)

    if is_training:
        bn_dense = keras.layers.Dropout(rate=hp.dense_dropout)(bn_dense)

    return keras.layers.Dense(
        # activation=tf.sigmoid,
        units=config['numOutputNeurons'],
        kernel_initializer=tf.contrib.layers.xavier_initializer())(bn_dense)


def build_model(x, is_training, config):
    '''Function implementing an example of how to build a model with the functions above.
    - 'x': placeholder whith the input.
    - 'is_training': placeholder indicating weather it is training or test phase, for dropout or batch norm.
    - 'config': dictionary with some configurable parameters like: number of output units - config['numOutputNeurons']
                or number of frequency bins of the spectrogram config['setup_params']['yInput']
    '''
    # The following line builds the model that achieved better results in our experiments.
    # It is based on a spectrogram front-end (num_filters=16) with 500 output units in the dense layer.
    return backend(wave_frontend(x, is_training), is_training, config, 500)
