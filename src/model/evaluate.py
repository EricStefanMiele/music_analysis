import tensorflow as tf

from Path import Path
from Utils.utils import f1_score
from model.hyperparams import Hyperparams as hp
from model.model import Model
import numpy as np


def eval_on_dev(config, feature_shape):
    # Define current graph
    graph = tf.Graph()

    with tf.Session(graph=graph, config=config) as sess:
        model = Model(graph, feature_shape[1], training=False)

        try:
            model.saver.restore(sess, tf.train.latest_checkpoint(Path.CHKP_DIR))
            print("Model successfully loaded.")
        except:
            print("Unable to load model, initializing variables from scratch.")
            return

        sess.run([model.iterator_init_op, tf.local_variables_initializer()])

        step = 0
        loss_sum = 0

        auc_roc_all = list()
        auc_pr_all = list()

        num_correct_predictions = 0
        num_total_predictions = 0

        saving_step = 2

        # same thing as train
        while True:
            with tf.device("/gpu:0"):

                try:
                    batch_loss, auc_roc, auc_pr, correct_preds, num_preds = sess.run(
                        [model.loss, model.auc_roc, model.auc_pr, model.correct_predictions,
                         model.num_predictions])
                    # feat,label= sess.run([model.features,model.labels])
                except tf.errors.OutOfRangeError:
                    print()
                    # when iterator has no more objects
                    break

                loss_sum += batch_loss
                avg_loss = loss_sum / (step + 1)

                auc_roc_all.append(float(auc_roc))
                auc_pr_all.append(float(auc_pr))

                num_correct_predictions += correct_preds
                num_total_predictions += num_preds

                # if time to save
                if step % saving_step == 0:
                    feed_dict = {
                        model.avg_loss: avg_loss,
                        model.auc_roc_ph: np.mean(auc_roc_all),
                        model.auc_pr_ph: np.mean(auc_pr_all),
                        model.accuracy_ph: num_correct_predictions / num_total_predictions,

                    }
                    # call log method
                    model.log_scores(sess, feed_dict, train_flag=False)

                print(
                    f"\rDEV Step {step + 1} - "
                    + f"Avg DEV loss: {round(avg_loss, 4)}"
                    + f" - ACCURACY train: {round(num_correct_predictions / num_total_predictions * 100, 2)}%"
                    + f" - AUC ROC: {round(np.mean(auc_roc_all), 4)}"
                    + f" - AUC PR: {round(np.mean(auc_pr_all), 4)}",
                    end="",
                )
                step += 1
