import os

import tensorflow as tf

from Path import Path
from model.hyperparams import Hyperparams as hp
from model.sound_model import build_model


class Model:
    def __init__(self, graph, feature_shape, training=True):
        self.writer = tf.summary.FileWriter(Path.LOGS_DIR, graph=graph)

        self.feature_shape = feature_shape
        self.global_step = tf.Variable(0, trainable=False, name='global_step')

        # if train load the train tfr
        if hp.mode == hp.PREDICT:
            dataset = tf.data.TFRecordDataset([Path.tfr_song_to_predict])
        else:
            if training:
                dataset = tf.data.TFRecordDataset([Path.tfr_train])
            else:
                dataset = tf.data.TFRecordDataset([Path.tfr_dev])

        # dataset=dataset.shuffle()
        # use map to extract info from record
        # split per batch size
        # dataset = dataset.map(map_func=self.extract_fn, num_parallel_calls=hp.num_parallel_calls)
        # dataset = dataset.batch(batch_size=hp.batch_size)
        dataset = dataset.prefetch(buffer_size=hp.prefetch_buffer_size)

        dataset = dataset.apply(tf.data.experimental.map_and_batch(
            map_func=self.extract_fn, batch_size=hp.batch_size,
            num_parallel_calls=hp.num_parallel_calls))

        # create iterator and initializator
        iterator = tf.data.Iterator.from_structure(dataset.output_types, dataset.output_shapes)
        self.iterator_init_op = iterator.make_initializer(dataset)

        # Get slice from iterator.
        self.features, self.labels = iterator.get_next()

        self.features = tf.cast(self.features, tf.float32)
        self.labels = tf.squeeze(self.labels)

        self.logits = build_model(self.features, is_training=training, config=hp.config)

        self.loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.labels, logits=self.logits)
        self.loss = tf.reduce_mean(self.loss)
        self.predictions = tf.nn.softmax(self.logits, axis=-1)  # tf.cast(tf.argmax(self.logits, 1), tf.int32)
        self.correct_predictions = tf.reduce_sum(
            tf.cast(
                tf.equal(
                    tf.cast(
                        tf.argmax(self.logits, 1),
                        tf.int32),
                    self.labels),
                tf.float32)
        )
        self.num_predictions = tf.shape(self.labels)[0]

        labels_one_hot = tf.one_hot(self.labels, 8)
        _, self.auc_roc = tf.metrics.auc(labels_one_hot, self.predictions)
        _, self.auc_pr = tf.metrics.auc(labels_one_hot, self.predictions, curve='PR')

        if training:
            optimizer = tf.train.AdamOptimizer(learning_rate=hp.learning_rate)
            #optimizer = tf.train.GradientDescentOptimizer(learning_rate=hp.learning_rate)
            # grads = optimizer.compute_gradients(self.loss)
            # clipped_grads = [(tf.clip_by_value(grad, -5., 5.), var) for grad, var in grads]
            # self.train_op = optimizer.apply_gradients(clipped_grads)

            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                self.train_op = optimizer.minimize(self.loss)  # ,global_step=self.global_step)

        with tf.name_scope("tensorboard"):
            self.avg_loss = tf.placeholder(tf.float32, shape=(), name="avg_loss")
            self.auc_roc_ph = tf.placeholder(tf.float32, shape=(), name="auc_roc_ph")
            self.auc_pr_ph = tf.placeholder(tf.float32, shape=(), name="auc_pr_ph")
            self.accuracy_ph = tf.placeholder(tf.float32, shape=(), name="accuracy_ph")

            self.metrics_summaries_train = [
                tf.summary.scalar("auc_roc_train", self.auc_roc_ph),
                tf.summary.scalar("auc_pr_train", self.auc_pr_ph),
                tf.summary.scalar("loss_train", self.avg_loss),
                tf.summary.scalar("accuracy_train", self.accuracy_ph),
            ]

            self.metrics_summaries_dev = [
                tf.summary.scalar("auc_roc_dev", self.auc_roc_ph),
                tf.summary.scalar("auc_pr_dev", self.auc_pr_ph),
                tf.summary.scalar("loss_dev", self.avg_loss),
                tf.summary.scalar("accuracy_dev", self.accuracy_ph),

            ]

            # Merge all summaries of the scores.
            self.merged_metrics_summaries_train = tf.summary.merge(self.metrics_summaries_train)
            self.merged_metrics_summaries_dev = tf.summary.merge(self.metrics_summaries_dev)

        # Create saver.
        self.saver = tf.train.Saver(max_to_keep=hp.max_model2keep)

        # Initialize all variables
        self.init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())

    def save_model(self, sess):
        """
        Saves the model.

        Args:
            sess: current Tensorflow session.
        """
        if not os.path.exists(Path.CHKP_DIR):
            os.makedirs(Path.CHKP_DIR)

        self.saver.save(sess, os.path.join(Path.CHKP_DIR, "model"), global_step=self.global_step)

        # if hp.compress_model:
        #     converter = tf.contrib.lite.TocoConverter.from_saved_model(Path.CHKP_DIR)
        #     converter.post_training_quantize = True
        #     tflite_quantized_model = converter.convert()
        #     open("quantized_model.tflite", "wb").write(tflite_quantized_model)

    def extract_fn(self, data_record):
        """
        Function to extract infos from frecord
        :param data_record:
        :return:
        """

        # get dict for extracting infos
        features = {
            # Extract features using the keys set during creation
            'x': tf.FixedLenSequenceFeature([self.feature_shape, hp.num_channels], tf.int64,
                                            allow_missing=True, ),
            'y': tf.FixedLenFeature([], tf.int64),
            # 'h': tf.FixedLenFeature([], tf.int64),
            # 'w': tf.FixedLenFeature([], tf.int64),
        }
        # extract single sample from whole record
        sample = tf.parse_single_example(data_record, features)

        # cast x to float and reshape it
        x = tf.cast(sample['x'], tf.float32)
        x = tf.reshape(x, (self.feature_shape, hp.num_channels))

        # same for label
        label = tf.cast(tf.reshape(sample['y'], shape=[-1]), dtype=tf.int32)

        return x, label

    def log_scores(self, sess, feed_dict, train_flag):
        """
        Log scores into tensorboard
        :param sess: the current session
        :param feed_dict: the dictionary to ffed
        :param train_flag: if train or dev
        :return:
        """

        if train_flag:
            summary = sess.run(
                self.merged_metrics_summaries_train,
                feed_dict=feed_dict,
            )
        else:
            summary = sess.run(
                self.merged_metrics_summaries_dev,
                feed_dict=feed_dict,
            )

        gs = tf.train.global_step(sess, self.global_step)
        self.writer.add_summary(summary, global_step=gs)

        sess.run(tf.assign_add(self.global_step, 1, ))
