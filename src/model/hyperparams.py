import os


class Hyperparams:
    # running params
    dev_size = 0.2
    batch_size = 16

    if 'eric' in os.getcwd():
        batch_size = int(batch_size/2)

    # Model params
    dense_dropout = 0.25
    learning_rate = 0.0001
    epochs = 10000

    # tensorflow params
    config = dict()
    config['numOutputNeurons'] = 8
    # config['setup_params'] = dict()
    # config['setup_params']['yInput'] = 128
    compress_model = True  # if compress model for evaluation
    load_model = True  # load model from checkpoint
    num_parallel_calls = 10
    prefetch_buffer_size = 10
    max_model2keep=4

    # Preprocessing params
    reduced_dim = 1000
    mono = False  # mono audio
    audio_max_dims = 239000  # max dims for audio signal
    msecs = 15000  # max time for audio track in millisecond
    chunks = 2  # splitting part for audio track

    # number of channels
    if not mono:
        num_channels = 2
    else:
        num_channels = 1

    downsample = True  # if to downsample audio
    downsample_method = "kaiser_fast"  # "kaiser_best","kaiser_fast","scipy"
    save_audio_debug = False  # if to save audio
    # original sample rate is 44100Hz
    down_sampling_rate = 16000  # rate of downsampling

    max_genre_count = -1
    min_genre_count = 100
    save_count = 1000  # index for saving step

    # In order to visualize every N steps the path and the genre of a song to check their consistency.
    show_path_genre_debug = True

    TRAIN = 0
    PREDICT = 1

    mode = TRAIN

    # front-end
    filters_fe = [64, 128]
    strides_fe = [3, 1]
    dropout_fe = 0.25

    # back-end
    filters_be = [64, 128]
    strides_be = [3, 1]
    dropout_be = 0.25

