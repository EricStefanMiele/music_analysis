import os.path
import wave

import librosa
import numpy as np
import pydub
import scipy
import scipy.signal
from scipy.io import wavfile

from Path import Path
from model.hyperparams import Hyperparams as hp


def save_debug_song(song_name, original, downsampled, original_rate, save_original=True, downsample_method=""):
    """
    Save a song for debugging reason
    :param song_name: the original name of the seong
    :param original: the data about the original song
    :param downsampled: the data about the downsampled song
    :param original_rate: the original rate
    :param save_original: if to save the original song
    :param downsample_method: the donsampled method name
    :return:
    """

    def write_wav(data, path, rate):
        out = wave.open(path, 'wb')
        out.setparams((2, 2, rate, 0, 'NONE', 'not compressed'))

        for i in range(0, len(data)):
            out.writeframes(bytes(data[i]))

        out.close()

    song_name = song_name.split(".")[0]

    original_path = os.path.join(Path.SONG_DIR, f"{song_name}_original.wav")
    if downsample_method == "": downsample_method = hp.downsample_method
    down_path = os.path.join(Path.SONG_DIR, f"{song_name}_{downsample_method}_downsampled.wav")

    if save_original:
        write_wav(original, original_path, original_rate)
    write_wav(downsampled, down_path, hp.down_sampling_rate)


def dowsample_librosa(data, old_samplerate, method):
    data = data.astype(np.float32).T
    data = librosa.resample(data, old_samplerate, hp.down_sampling_rate, res_type=method)
    data = data.astype(np.int16).T

    return data


def downsample_interpolate(data, old_samplerate):
    """
    Downsample a song through interpolation
    :param data: (numpy arrya9 the original song data
    :param old_samplerate: (int) the original sample rate
    :param new_sample_rate: (int) the desired sample rate
    :return: (numpy array) downsampled song
    """
    if old_samplerate != hp.down_sampling_rate:
        duration = data.shape[0] / old_samplerate

        time_old = np.linspace(0, duration, data.shape[0])
        time_new = np.linspace(0, duration, int(data.shape[0] * hp.down_sampling_rate / old_samplerate))

        interpolator = scipy.interpolate.interp1d(time_old, data.T)
        data = interpolator(time_new).T

    return data


def downsample_resample(data, old_samplerate):
    """
    Downsample a song through interpolation
    :param data: (numpy arrya9 the original song data
    :param old_samplerate: (int) the original sample rate
    :param new_sample_rate: (int) the desired sample rate
    :return: (numpy array) downsampled song
    """
    if old_samplerate != hp.down_sampling_rate:

        if len(data.shape) > 1:
            left = scipy.signal.resample(data[:, 0], 2)
            right = scipy.signal.resample(data[:, 1], 2)

            data = np.stack((left, right), axis=1)
        else:
            data = scipy.signal.resample(data, hp.down_sampling_rate)

    return data


def downsample_skip(data, old_samplerate):
    """
      Downsample a song through frame skipping
      :param raw_data: (numpy arrya9 the original song data
      :param old_samplerate: (int) the original sample rate
      :param new_sample_rate: (int) the desired sample rate
      :return: (numpy array) downsampled song
      """

    if old_samplerate == hp.down_sampling_rate:
        return data

    skipping_float = old_samplerate / hp.down_sampling_rate

    skipping = int(np.floor(skipping_float))

    final_dim = int(data.shape[0] / skipping_float)

    data = data[::skipping]
    data = data[:final_dim]

    return data


def extract_audio_features(path2song):
    """
    Extract the audio data from a song file
    :param path2song: (str) the paht to the song
    :param skip: (bool deafult False) which downsampler to use
    :return: (list of numpy arrays) songs
    """
    # read mp3 file
    try:
        mp3 = pydub.AudioSegment.from_mp3(path2song)
    except Exception as e:
        print(e)
        return None

    chunk_list = list()
    song_name = path2song.split("/")[-1]

    temp_file = os.path.join(Path.SONG_DIR, "tmp.wav")

    for i in range(hp.chunks):
        mp3_chunk = mp3[hp.msecs * i:hp.msecs * (i + 1)]
        # convert to wav
        mp3_chunk.export(temp_file, format="wav")
        # read wav file

        o_rate, data = wavfile.read(temp_file)

        if len(data) == 0:
            return None

        if hp.mono:
            data = np.mean(data, axis=1)
            data=np.expand_dims(data,axis=1)

        if hp.downsample:
            data = dowsample_librosa(data, o_rate, method=hp.downsample_method)

        # cut off the data (make the chunk of approximately 14 secs)
        data = data[:hp.audio_max_dims]

        if hp.save_audio_debug:
            save_debug_song(song_name,None,data,o_rate,save_original=False,downsample_method=hp.downsample_method)

        chunk_list.append(data)

    chunk_list = np.array(chunk_list)
    if len(data.shape) < 2:
        if hp.mono:
            chunk_list = np.expand_dims(chunk_list, axis=2)
        else:
            return None

    chunk_list = np.asarray(chunk_list)
    return chunk_list
