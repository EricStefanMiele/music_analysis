import gc
import pprint
import math
from collections import Counter

import numpy as np
import tensorflow as tf
from pandas import DataFrame
from tqdm import tqdm, trange

from Path import Path
from Utils.data import vocabulary2index
from Utils.debug import perc_print
from Utils.files import get_number_of_files, get_files_with_name
from Utils.serialization import load_pkl, dump_pkl
from data import dataset_utils
from data.signal_processing import extract_audio_features
from model.hyperparams import Hyperparams as hp


def get_data():
    """
    Main function called to check if data is already generated
    If so then load it and return, else generate it
    :return: feature shape, genre dict
    """
    # if no data is avaiable generate it
    if get_number_of_files(Path.TMP_DIR) == 0:
        return generate_dataset()

    # same thing for tensorflow stuff
    if get_number_of_files(Path.TF_DIR) == 0:
        return generate_dataset()

    # get the first feature from the temp file and load it
    feat = get_files_with_name(Path.TMP_DIR, Path.tmp_feats)[0]
    feat = load_pkl(feat)

    # same thing for the dict
    genre_dict = load_pkl(Path.genre_dict)

    # if some of them is None, generate data
    if feat is None or genre_dict is None:
        return generate_dataset()

    return np.asarray(feat[0]).shape, genre_dict


def generate_dataset():
    """
    Pipeline for generating dataset
    :return:
    """

    # get infos about genre
    genres_count, track_paths_list, genres_list = genre_count()

    # pass it to process and get genre dict
    genre_dict = process_data(genres_count, track_paths_list, genres_list)

    # finally prepare data
    feature_shape = prepare_data_record()

    # return
    return feature_shape, genre_dict


def genre_count():
    """
    Read the dataset using fma class and return genre/path list
    :return:
    """

    def load_dataset():
        """
        Load the dataset from the tracks.csv
        :return:
        """
        tracks = dataset_utils.load(Path.meta_tracks)
        dataset = tracks[tracks["set", "subset"] <= "small"]
        return dataset

    # load data from pickle
    dataset = load_pkl(Path.tmp_dataset)
    if dataset is None:
        # else use fma class
        dataset = load_dataset()
        dump_pkl(dataset, Path.tmp_dataset)

    # define a dataframe and build paths
    track_ids = DataFrame(dataset.index.get_level_values("track_id"))
    track_paths = track_ids.applymap(lambda track_id: dataset_utils.get_audio_path(Path.small_folder, track_id))

    track_paths = track_paths.values.tolist()
    genres = dataset["track"]["genre_top"].values.tolist()

    shuffled = list(zip(track_paths, genres))
    # shuffle
    np.random.seed(42)
    np.random.shuffle(shuffled)
    track_paths, genres = zip(*shuffled)

    track_paths_list = list()
    genres_list = list()
    genres_count = list()

    # build paths
    for i in range(len(genres)):
        genre = genres[i]

        track_paths_list.append(track_paths[i])
        genres_list.append(genre)
        genres_count.append(genre)
        perc_print(i, len(genres), "genres computed1")

    print(f"\nNum of tracks: {len(track_paths_list)}")

    return genres_count, track_paths_list, genres_list


def process_data(genres_count, track_paths_list, genres_list):
    """
    Function to actually generate the dataset and save it
    :return:
    """

    # use pp for printing dicts
    pp = pprint.PrettyPrinter(indent=4)

    # generate dictionary from list
    genres_count = dict(Counter(genres_count))
    _, genre2index, index2genre = vocabulary2index(list(genres_count.keys()))
    genre_dicts = (genre2index, index2genre)
    # and save them
    dump_pkl(genre_dicts, Path.genre_dict)

    # initialize lists
    features = []
    labels = list()
    # track_paths_filtered = list()

    # get incremental dict from pickle
    incremental_genres_count_loaded = load_pkl(Path.tmp_genre_count)

    incremental_genres_count = dict()

    # initialize counters
    count = 0  # count how many song have been saved
    skipped = {'min': 0, 'max': 0, 'dimension': 0, 'shape': 0}  # dict for debug

    for i in tqdm(range(len(genres_list))):

        # get the genre fo the current track
        genre = genres_list[i]

        # If the process of generation has been restored.
        if incremental_genres_count_loaded is not None:
            try:
                # If the current count is less than the previous restored count, increase the current count.
                if incremental_genres_count[genre] < incremental_genres_count_loaded[genre]:
                    incremental_genres_count[genre] += 1
                    count += 1
                    continue
            except KeyError:
                incremental_genres_count[genre] = 1
                count += 1
                continue

        # If the current genre has not enough elements.
        if genres_count[genre] < hp.min_genre_count:
            skipped['min'] += 1
            continue

        # check if we reached the maximum amount for genre
        try:
            if hp.max_genre_count > -1:
                if incremental_genres_count[genre] >= hp.max_genre_count:
                    skipped['max'] += 1
                    continue
        except KeyError:
            incremental_genres_count[genre] = 0

        # Current track path.
        track_path = track_paths_list[i]

        # extract feature from song
        feats = extract_audio_features(track_path[0])

        # Show the path and the genre for the current track.
        if hp.show_path_genre_debug and count % 50 == 0:
            print(f'\nrhythmbox {track_path[0]}')
            print(genre)

        # if feature is None then an error occurred
        if feats is None:
            skipped['dimension'] += 1
            continue

        # if the feature does not have the downsampled size in it then an error occurred
        if hp.downsample:
            if len(feats.shape) != 3 or feats.shape[1] != hp.audio_max_dims or feats.shape[2] != hp.num_channels:
                skipped['shape'] += 1
                continue

        # add incremental for genre
        try:
            incremental_genres_count[genre] += 1
        except KeyError:
            incremental_genres_count[genre] = 1

        # append track path and genre for current audio track.
        # track_paths_filtered.append(track_path)
        labels.append(genre2index[genre])

        # append feature
        features.append(feats)

        # increase count
        count += 1

        # if iks time to save
        if count % hp.save_count == 0 and count != 0:
            # dump the three lists
            dump_pkl(features, Path.tmp_feats + f"_{count}.pkl")
            dump_pkl(labels, Path.tmp_labels + f"_{count}.pkl")
            # dump_pkl(track_paths_filtered, Path.tmp_paths + f"_{count}.pkl")
            # and reset them
            features = []
            labels = list()
            # track_paths_filtered = list()
            # dump the updated genre count
            dump_pkl(incremental_genres_count, Path.tmp_genre_count)
            pp.pprint(incremental_genres_count)
            gc.collect()

    # Dump the last chunk.
    if len(features) >= hp.batch_size:
        # dump the three lists
        dump_pkl(features, Path.tmp_feats + f"_{count}.pkl")
        dump_pkl(labels, Path.tmp_labels + f"_{count}.pkl")
        # dump_pkl(track_paths_filtered, Path.tmp_paths + f"_{count}.pkl")
        # dump the updated genre count
        dump_pkl(incremental_genres_count, Path.tmp_genre_count)
        pp.pprint(incremental_genres_count)

    # free memory
    del track_paths_list
    del genres_count
    del genres_list
    gc.collect()

    # print infos
    skipped['count'] = count
    pp.pprint(skipped)
    pp.pprint(incremental_genres_count)

    # return genre dict
    return genre_dicts


def prepare_data_record():
    """
    Write the data from pickle to tf record
    :return:
    """

    # load path lists
    features = get_files_with_name(Path.TMP_DIR, Path.tmp_feats)
    labels = get_files_with_name(Path.TMP_DIR, Path.tmp_labels)

    # sort them
    features = sorted(features)
    labels = sorted(labels)

    # merge
    shuffled = list(zip(features, labels))
    # shuffle
    np.random.shuffle(shuffled)
    # define split index
    split_idx = math.floor(len(shuffled) * (1 - hp.dev_size))

    # get them
    train = shuffled[:split_idx]
    dev = shuffled[split_idx + 1:]

    print(f'Train data shape: {np.asarray(train).shape}')
    print(f'Dev data shape: {np.asarray(dev).shape}')

    # write in tf record form
    npy_to_tfrecords(train, Path.tfr_train)
    npy_to_tfrecords(dev, Path.tfr_dev)

    features = load_pkl(features[0])[0]
    # return feature shape
    return features.shape


def load_pickles(sub_name):
    """
    Load already processed pickles
    :param sub_name:
    :return:
    """
    paths = get_files_with_name(Path.TMP_DIR, sub_name)
    res = []

    for p in paths:
        res += load_pkl(p)

    return res


def npy_to_tfrecords(paths, file_name):
    """
    Write dataset into tf record
    :param paths: zipped list containing:

        feature_paths: list of paths to pickled list of features
        label_paths: list of paths to pickled list of lables

    :param file_name: the name to use for the tf record
    :return:  None
    """
    # initialize writer
    writer = tf.python_io.TFRecordWriter(file_name)

    # Loop through all files
    for f_path, l_path in tqdm(paths, desc="feature pack"):

        # X has shape [num of samples, chunks, features , channels]
        X = load_pkl(f_path)
        # y has shape [num of samples * chunks]
        y = load_pkl(l_path)

        print(np.asarray(X).shape)
        print(np.asarray(y).shape)

        # loop over all features
        for i in trange((len(X)), desc="features", leave=False):
            # get the song with dimension [chunks, features, channels]
            x_song = X[i]

            # for every chunk
            for j in range(len(x_song)):
                x = x_song[j]

                feature_dict = dict()
                # x must me flatten from 2 dimension to 1
                feature_dict['x'] = _int64_feature(x.flatten())
                feature_dict['y'] = _int64_feature([y[i]])

                features = tf.train.Features(feature=feature_dict)

                # Construct the Example proto object
                sample = tf.train.Example(features=features)

                # Serialize the example to a string
                serialized = sample.SerializeToString()

                # write the serialized objec to the disk
                writer.write(serialized)

    writer.close()


def npy_to_tfrecords_single_sample(song, file_name):
    # initialize writer
    writer = tf.python_io.TFRecordWriter(file_name)

    # get the song with dimension [features, channels]
    song_chunk = song[np.random.randint(0, 2)]

    feature_dict = dict()
    # x must me flatten from 2 dimension to 1
    feature_dict['x'] = _int64_feature(song_chunk.flatten())

    # Fake, placeholder.
    feature_dict['y'] = _int64_feature([0])

    features = tf.train.Features(feature=feature_dict)

    # Construct the Example proto object
    sample = tf.train.Example(features=features)

    # Serialize the example to a string
    serialized = sample.SerializeToString()

    # write the serialized objec to the disk
    writer.write(serialized)

    writer.close()


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


