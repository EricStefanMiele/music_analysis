import os
import shutil

import tensorflow as tf

from Path import Path
from data.generation import get_data
from model.train import train
from model.predict import predict
from model.hyperparams import Hyperparams as hp


def main():
    # In order to delete all old logs for memory reasons
    if not os.path.exists(Path.LOGS_DIR):
        os.makedirs(Path.LOGS_DIR)
    else:
        shutil.rmtree(Path.LOGS_DIR)
        os.makedirs(Path.LOGS_DIR)

    # In order to allow GPU memory growth
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    # Reset the graph
    tf.reset_default_graph()

    feature_shape, genre_dict = get_data()

    if hp.mode == hp.TRAIN:
        train(config, feature_shape, len(genre_dict[0].keys()))
    if hp.mode == hp.PREDICT:
        predict(config, feature_shape, len(genre_dict[0].keys()))


if __name__ == "__main__":
    main()
