import os
import shutil
from os import curdir
from os.path import join


class Path:
    DATA_DIR = join(curdir, "../Dataset")

    MUSIC_DATA_DIR = join(DATA_DIR, "music_dataset")

    small_folder = join(MUSIC_DATA_DIR, "fma_small")
    meta_folder = join(MUSIC_DATA_DIR, "fma_metadata")
    meta_tracks = join(meta_folder, "tracks.csv")

    CUSTOM_DIR = join(DATA_DIR, "Custom")
    dataset_train = join(CUSTOM_DIR, "dataset_train.pkl")
    dataset_dev = join(CUSTOM_DIR, "dataset_dev.pkl")
    genre_dict = join(CUSTOM_DIR, "genre_dict.pkl")

    TF_DIR = join(CUSTOM_DIR, "Tensorflow")
    tfr_train = join(TF_DIR, "tfr_train.tfrecords")
    tfr_dev = join(TF_DIR, "tfr_dev.tfrecords")

    TMP_DIR = join(CUSTOM_DIR, "Tmp")
    tmp_feats = join(TMP_DIR, "tmp_feats")
    tmp_labels = join(TMP_DIR, "tmp_labels")
    tmp_paths = join(TMP_DIR, "tmp_paths")
    tmp_dataset = join(TMP_DIR, "tmp_dataset.pkl")
    tmp_genre_count = join(TMP_DIR, "genre_count.pkl")

    DEBUG_DIR = join(curdir, "../Debug")
    LOGS_DIR = join(DEBUG_DIR, "logs")
    CHKP_DIR = join(DEBUG_DIR, "model_checkpoints")
    SONG_DIR = join(DEBUG_DIR, "songs")
    SONG_TO_PREDICT_DIR = join(DEBUG_DIR, 'song_to_predict')

    song_to_predict = join(SONG_TO_PREDICT_DIR, 'song.mp3')
    tfr_song_to_predict = join(SONG_TO_PREDICT_DIR, 'tfr_song_to_predict.tfrecords')

    def __init__(self):
        self.empty_dirs([self.LOGS_DIR,self.SONG_DIR])
        self.initialize_dirs()

    def initialize_dirs(self):
        """
        Initialize all the directories  listed above
        :return:
        """
        variables = [attr for attr in dir(self) if not callable(getattr(self, attr)) and not attr.startswith("__")]
        for var in variables:
            if var.endswith('DIR'):
                path = getattr(self, var)
                if not os.path.exists(path):
                    os.makedirs(path)

    def empty_dirs(self, to_empty):
        """
        Empty all the dirs in to_empty
        :return:
        """

        for folder in to_empty:
            try:
                for the_file in os.listdir(folder):
                    file_path = os.path.join(folder, the_file)
                    try:
                        if os.path.isfile(file_path):
                            os.unlink(file_path)
                        elif os.path.isdir(file_path):
                            shutil.rmtree(file_path)
                    except Exception as e:
                        print(e)
            except Exception:
                continue


Path()
