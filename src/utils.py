import pickle

from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer


def save_obj(obj, path):
    """Saves the obj on file in pickle format.

    Arguments:
        obj: object to save on file.
        path (str): path where to save the obj.
    """
    with open(path, 'wb') as file:
        pickle.dump(obj, file, pickle.HIGHEST_PROTOCOL)


def load_obj(path):
    """Restores the object.

    Returns:
        restored object.
    """
    with open(path, 'rb') as file:
        return pickle.load(file)


def dim_reduction_svd(vectors, reduced_dims):
    """
    Applies dimensionality reduction to the vectors through SVD.

    Args:
        vectors: tfidf vector representation of docs.
        reduced_dims (int): reduced targed dimension.

    Returns:
        reduced truncated matrix.
    """
    svd = PCA(n_components=reduced_dims, random_state=42 * 2311 - 2312)
    normalizer = Normalizer(copy=False)
    transformer = make_pipeline(svd, normalizer)
    vectors_transformed = transformer.fit_transform(vectors)
    return vectors_transformed, transformer
